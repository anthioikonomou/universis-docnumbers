import {DataObject} from '@themost/data'
/**
 * @class
 */
declare class DocumentNumberSeriesItem extends DataObject {
     
    public id: number; 
    public additionalType?: string; 
    public alternateName: string; 
    public contentType: string; 
    public datePublished?: Date; 
    public published: boolean; 
    public keywords?: string; 
    public thumbnail?: string; 
    public version?: string; 
    public attachmentType?: any; 
    public description?: string; 
    public image?: string; 
    public name?: string; 
    public url?: string; 
    public dateCreated?: Date; 
    public dateModified?: Date; 
    public createdBy?: any; 
    public modifiedBy?: any; 
    public parentDocumentSeries: any;
    public documentNumber: string;
    public documentStatus?: any;
    public signed?: boolean;
    replace(data: Buffer | ArrayBuffer | ReadableStream, attributes: any): Promise<DocumentNumberSeriesItem>;

}

export default DocumentNumberSeriesItem;